

var input = {
  word: 'ABCDE',
  height: 4,
  width: 3,
};


function drawWord(input) {
  if (!input.word  || input.word.length <= 0 ||
     !input.height || !input.width)
    return;

  // per row
  for (var i = 0; i < input.height; i++) {
    // per line
    var  j = ( i > 0) ? 1: 0;
    for(; j < input.word.length; j++) {
      // per block
      var word = '';
      for (var k = 0 ; k < input.width; k++) {
        if (j === 0 || j === (input.word.length -1) ) {
          // first line / last line
          var isOdd = (input.word.length - 1 === j) ? 0: 1;
          //console.log( isOdd + '' + k);
          if ((i + isOdd  + k ) % 2 ) {
            word += ( k > 0) ? input.word.substring(1) : input.word;
          } else {
            var reversedWord = input.word.split("").reverse().join("");
            word += ( k > 0) ?
              reversedWord.substring(1) : reversedWord;
          }
        } else {
          // middle
          if (k <= 0) {
            // first block
            word += ((i + k) % 2) ?
              input.word[input.word.length - 1 -j] : input.word[j];
          }
          for (var re = 0 ; re < input.word.length - 2; re ++) {
            word += ' ';
          }
          word += ((i + k) % 2) ?
            input.word[j] : input.word[input.word.length - 1 - j];
        }
      }
      console.log(word);
    }
  }
}


drawWord(input);
