## Description ##
There is a crisis unfolding in Reddit. For many years, Redditors have continued to evolve sh*tposting to new highs, but it seems progress has slowed in recent times. Your mission, should you choose to accept it, is to create a state of the art rektangular sh*tpost generator and bring sh*tposting into the 21st century.
Given a word, a width and a length, you must print a rektangle with the word of the given dimensions.

## Formal Inputs & Outputs ##
# Input Description #
The input is a string word, a width and a height

# Outputs Description #
Quality rektangles. See examples. Any orientation of the rektangle is acceptable

# Examples #
* Input: "REKT", width=1, height=1
Output:   
R E K T  
E &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; K  
K  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   E  
T K E R  

* Input: "REKT", width=2, height=2  
Output:  
T K E R E K T  
K  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   E &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    K          
E &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    K  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   E  
R E K T K E R  
E &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    K  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   E  
K  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   E  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   K  
T K E R E K T  

## Bonus ##
Many fun bonuses possible - the more ways you can squeeze REKT into different shapes, the better.
* Print rektangles rotated by 45 degrees.
* Print words in other shapes (? surprise me)
* Creatively colored output? Rainbow rektangles would be glorious.
