
// input for first problem
var input1 = [ 3, 3, 2, 1, 2, 2, 4, 2, 3, 1 ,5];

// input for second problem
var input2 = [['a', 1], ['a', 10],
['b', 21], ['b', 11], ['a', 33], ['c', 12], ['b', 7],
['d', 6]];

function solution1(input) {
  var numCount = [];
  var index = [];
  input.forEach(function(num) {
    if (typeof(numCount[num]) != 'undefined') {
      numCount[num]++;
    } else {
      index.push(num);
      numCount[num] = 0;
    }
  });
  var output = [];
  index.forEach(function(num) {
    output.push({
      'number': num,
      'count': numCount[num],
    });
  });
  return output;
}

function solution2(input) {
  var numCount = {};
  var index = [];
  input.forEach(function(num) {
    if (typeof(numCount[num[0]]) != 'undefined') {
      numCount[num[0]] += num[1];
    } else {
      index.push(num[0]);
      numCount[num[0]] = num[1];
    }
  });
  var output= [];
  index.forEach(function(num){
    output.push({
      'key': num,
      'sum': numCount[num],
    });
  });
  return output;
}

console.log(solution1(input1));

console.log(solution2(input2));
