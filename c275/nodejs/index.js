
// test in mocha
var request = require('request');
var assert = require('chai').assert;
var allElemetFullName = null;
var result = [];
function duplicated() {
    for(var i =0; i< result.length; i++) {
      if (result.indexOf(result[i]) !=
         result.lastIndexOf(result[i])) {
           return false;
         }
    }
    return true;
}
function checkValid(fullName, shortName) {
  if (shortName.length != 2)
    return false;
  if (fullName.toLowerCase().indexOf(shortName[0]) < fullName.toLowerCase().lastIndexOf(shortName[1]))
    return true;
  console.log(fullName + ' ' + shortName);
  return false;
}
describe('All test', function() {
  it('getting the data', function(done) {
    request('http://pastebin.com/raw/uVyHtMRb',
      function(err, resp, body) {
        assert.equal(200, resp.statusCode);
        assert.equal(null, err);
        allElemetFullName = body.split('\r\n');
        done();
    });
  });
  it('checking all the data', function(done) {
    assert.equal(366, allElemetFullName.length);
    done();
  });
  it('process the solution', function(done) {
    assert.equal(true, solution1());
    done();
  });
  describe('check result', function() {
    it('checking the result duplicated', function(done) {
      assert.equal(true,duplicated());
      done();
    });
    it('check the result length', function() {
      assert.equal(allElemetFullName.length, result.length);
    });
    it('check each result valid', function() {
      for (var i = 0; i < result.length; i++) {
        assert.equal(true, checkValid(allElemetFullName[i], result[i]));
      }
    });

  });
});


function solution1() {
  // generate all combination
  function findNumOfValidSymbol(fullName) { // from easy c275
    fullName = fullName.toLowerCase();
    var tmp = [];
    for(var i = 0; i < fullName.length; i++) {
      for(var j = i + 1; j < fullName.length; j++) {
        var short = fullName.charAt(i) + fullName.charAt(j);
        if (tmp.indexOf(short) < 0) {
          tmp.push(short);
        }
      }
    }
    //O(N^3)
    return tmp;
  };
  var resultsSymbol = {};
  function numberOfAvailSymbol(element) {
    var count = 0;
    for (var i = 0; i < element.all.length; i++) {
      if (!resultsSymbol[element.all[i]])
        count++;
    }
    return count;
  }
  function insertSymbol(element, beginning) {
    for (var j = 0; j < element.all.length; j++) {
      if (element.fullName == 'Bartium') {
      }
      if (!resultsSymbol[element.all[j]]) {
        element.index  = j;
        resultsSymbol[element.all[j]] = element;
        return element.all[j];
      }
    }
    for (var j = 0; j < element.all.length; j++) {
      if (numberOfAvailSymbol(resultsSymbol[element.all[j]]) > 0) {
        var ele = resultsSymbol[element.all[j]];
        resultsSymbol[element.all[j]] = element;
        element.index = j;
        return insertSymbol(ele, false);
      }
    }
    return false;
  }
  var allE = [];
  for(var i =0 ; i < allElemetFullName.length; i++) {
    var element = {
      fullName: allElemetFullName[i],
      all: findNumOfValidSymbol(allElemetFullName[i]),
      index: 0,
    };
    allE[i] = element;
    var selectSym = insertSymbol(element, true);
    if (selectSym == false) {
      return false;
    }
  }
  for (var i = 0 ; i < allElemetFullName.length; i++) {
    result[i] = allE[i].all[allE[i].index];
  }
  return true;
}
