
var fullName = 'Melintzum';
var shortName = 'Nn';

function checkValid(fullName, shortName) {
    var l_full = fullName.toLowerCase();
    var l_short = shortName.toLowerCase();
    if (shortName.length != 2) {
      return "false";
    }
    if (l_full.indexOf(l_short.charAt(0)) <
      l_full.lastIndexOf(l_short.charAt(1))) {
      return "true";
    }
    return "false";
};

console.log(checkValid(fullName, shortName));

function findNumOfValidSymbol(fullName) {
  // stupid method first
  fullName = fullName.toLowerCase();
  var tmp = [];
  for(var i = 0; i < fullName.length; i++) {
    for(var j = i + 1; j < fullName.length; j++) {
      var short = fullName.charAt(i) + fullName.charAt(j);
      if (tmp.indexOf(short) < 0) {
        tmp.push(short);
      }
    }
  }
  //O(N^3)
  return tmp.length;
};

console.log(findNumOfValidSymbol("nanen"));
