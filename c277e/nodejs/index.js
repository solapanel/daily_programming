
var input = [[4, 8], [4096, 1024],
            [7673, 4729], [46410, 119340]];
var input2 = {
  setter: [['x', 'cb'],
          ['y', 'ab'],
          ['z', 'xa']],
  data: [['ab', 'cb'],
        ['ab', 'x'],
        ['x', 'y'],
        ['z', 'y'],
        ['z', 'xay']],
};

function gcd(a, b) {
  if (a < b) {
    var tmp = a;
    a = b;
    b = tmp;
  }
  if (b === 0) {
    return a;
  }
  return gcd(b, a% b);
}

function solution(input) {
  input.forEach(function(set) {
    var a = set[0];
    var b = set[1];
    var common = gcd(a,b);
    console.log(a/common + ' ' + b/common);
  });
}

function gcdtext(text1, text2) {
  var ret = '';
  for (var i =0; i < text1.length; i++) {
    var c = text1.charAt(i);
    var index= text2.indexOf(c);
    if (index >= 0){
      var text2New = text2.slice(index+1);
      if (index > 0) {
        text2New += text2.slice(0, index);
      }
      text2 = text2New;
    } else {
      ret += c;
    }
  }
  if (text2.length === 0){
      text2 = '1';
  }
  if (ret.length === 0){
      ret = '1';
  }
  return [ret, text2];
}

function solution2(input) {
  // replace all string
  var stop = false;
  var i = 0,
    j = 0;
  while(! stop) {
    stop = true;
    for (i =0; i < input.data.length; i++) {
      for (j = 0; j < input.setter.length; j++) {
        if (input.data[i][0].indexOf(input.setter[j][0]) >=0) {
          input.data[i][0] =
            input.data[i][0].replace(input.setter[j][0], input.setter[j][1]);
          stop = false;
        }
        if (input.data[i][1].indexOf(input.setter[j][0]) >=0) {
          input.data[i][1] =
            input.data[i][1].replace(input.setter[j][0], input.setter[j][1]);
          stop = false;
        }
      }
    }
  }
  for (i = 0; i < input.data.length; i++) {
    console.log(gcdtext(input.data[i][0], input.data[i][1]));
  }
}


solution(input);
solution2(input2);
