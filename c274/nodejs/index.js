var input = [
    [6, 12, 6, 12],
    [5, 7, 13, 5],
    [3, 9, 3, 9],
];

function checkValid(input) {
  for (var i = 0 ; i < input.length; i++) {
      for (var j =0; j < input[i].length; j++) {
        //console.log('' + i + '' + j);
        if (input[i][j] & 1) { // up
          if (!(input[i-1][j] & 4))
            return false;
        }
        if (input[i][j] & 4) { // down
          if (!(input[i+1][j] & 1))
            return false;
        }
        if (input[i][j] & 2) { // right
          if (!(input[i][j+ 1] & 8))
            return false;
        }
        if (input[i][j] & 8) { // left
          if (!(input[i][j -1 ] & 2))
            return false;
        }
      }
  }
  return true;
}

console.log(checkValid([
  [6, 12, 6, 12],
  [5, 7, 13, 5],
  [3, 9, 3, 9]
]));

console.log(checkValid([[0, 2, 8, 0]]));

function allCombination() {

}
